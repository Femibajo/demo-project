// SELECT elements

const iconElement = document.querySelector(".weather-icon");
const tempElement = document.querySelector(".temperature-value p");
const descElement = document.querySelector(".temperature-description p");
const locationElement = document.querySelector(".location p");
const notificationElement = document.querySelector(".notification");

// creating variable for the app data for values fetched from the api
const weather = {};

weather.temperature = {
    unit : "celsius"
}

// app consts and vars
const KELVIN = 273;
// API key
const key = "b65cb11e00111d72b891738c494409d2";

// check if browser supports geolocation
if('geolocation' in navigator){
    navigator.geolocation.getCurrentPosition(setPosition, showError);
}else {
notificationElement.style.display = "block";
notificationElement.innerHTML = "<p>Browser doesn't support Geolocation</p>";
    }

// set user's position
function setPosition(position){
    let latitude = position.coords.latitude;
    let longitude = position.coords.longitude;

    getWeather(latitude, longitude);

    
}
// show error when there are issues with geolocation services
function showError(error){
notificationElement.style.display = "block";
notificationElement.innerHTML = `<p>${error.message}</p>`;
}

// get weather from api provider
function getWeather(latitude, longitude){
    let api = `http://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${key}`;
    


// fetching the api
    fetch(api)
        .then(function(response){
            let data = response.json();
            return data;
            console.log(data);
        })
        .then(function(data){
            weather.temperature.value = Math.floor(data.main.temp-KELVIN);
            weather.description = data.weather[0].icon;
            weather.iconID = data.weather[0].icon;
            weather.city = data.name;
            weather.country = data.sys.country;
        })
        .then(function(){
            displayWeather();
        });
}
// displaying weather to user interface

function displayWeather(){
    iconElement.innerHTML = `<img src="icons/${weather.iconID}.png"/>`;
    tempElement.innerHTML = `${weather.temperature.value}o<span>C</span>`;
    descElement.innerHTML = weather.description;
    locationElement.innerHTML = `${weather.city}, ${weather.country}`; 
}

// temperature conversion C to F
function celsiusToFahrenheit(temperature){
    return (temperature *9/5) + 32;
}
// when user clicks on temperature element
tempElement.addEventListener("click", function(){
    console.log(weather.temperature.value);
    if(weather.temperature.value === undefined) return;
    if(weather.temperature.unit == "celsius"){
        let fahrenheit = celsiusToFahrenheit(weather.temperature.value);
        fahrenheit = Math.floor(fahrenheit);

        tempElement.innerHTML = `${fahrenheit}<sup>o</sup><span>F<span>`;
        weather.temperature.unit = "fahrenheit";

    }else {
        tempElement.innerHTML = `${weather.temperature.value}<sup>o</sup><span>C</span>`;
        weather.temperature.unit = "celsius";
    }
});


